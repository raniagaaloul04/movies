import './App.css';
import { Routes, BrowserRouter as Router, Route } from 'react-router-dom'
import Home from './Components/Home/Home.Component';



function App() {
  return (
    <div className="App">
    <Router>
    <Routes>
      <Route path="/" element={<Home/>}></Route>
    </Routes>
  </Router>

    </div>
  );
}

export default App;
