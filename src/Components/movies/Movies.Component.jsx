import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import movies from '../../movies'
import { Grid, Pagination, TablePagination } from '@mui/material';
import IconButton, { IconButtonProps } from '@mui/material/IconButton';
import FavoriteIcon from '@mui/icons-material/Favorite';
import DeleteIcon from '@mui/icons-material/Delete';
import { red } from '@mui/material/colors';
import { useState } from 'react';
import { Route } from 'react-router-dom';

// import img from  "../../images/1.jpg";
const Movies = () => {
const [like,setlike] =useState();
const [state,setState] =useState();
const [page,setPage] =React.useState(0);

const [RowsPerPage,setRowsPerPage] =React.useState(0);

const [dislike,setdislike] =useState();
const [likeactive,setlikeactive] =useState();
const [dislikeactive,setdislikeactive] =useState();

const handleChangePage=(event,newPage)=>{
setPage(newPage);} 

const handleChangeRowPerpage=(event)=>{
  setRowsPerPage(parseInt(event.target.value),10);
  setPage(0)} 
function likef(id)
{ if(likeactive)
{
setlikeactive(false)
setlike(like-1)}
else{
setlikeactive(true)
setlike(like+1)
  if(dislikeactive)
  {
setdislikeactive(false)
setlike(like+1)
setdislike(dislike-1)}
 }}

function dislikelikef()
{ if(dislikeactive)
  {
  setdislikeactive(false)
  setdislike(dislike-1)}
  else{
  setdislikeactive(true)
  setdislike(dislike+1)
    if(likeactive)
    {
  setlikeactive(false)
  setdislike(dislike+1)
  setlike(like-1)}
   }}

function DeleteMovie(id)
{ 
  // this.props.setState({
  //   ItemsArray: this.state.ItemsArray.filter((x) => x.id != id )
  // });
  fetch('../../movies.js',{
  method:'DELETE'
}).then((result)=>{ 
  result.json().then((res)=>{console.warn(res );})
})
}

  return (
    <div style={{marginLeft:300}}>

         
   
    <Grid container spacing={{ xs: 2, md: 3 }} columns={{ xs:14, sm: 8, md: 12 }}>




            {
           
              movies.map(item => {
                return (

                  <div class="card1"  style={{marginTop:100}}>

                  <Grid xs={2} sm={4} md={10} >
                  <Card sx={{ maxWidth: 445}}>
                  <CardMedia
                    component="img"
                    height="480"
                    src={require('../../images/'+item.thumbnail)} 
                                alt="green iguana"
                  />

                  <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                    {item.title}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                    {item.category}
                    </Typography>
                  </CardContent>
                  <CardActions>
       
<button onClick={e => DeleteMovie(item._id)}>        
<IconButton aria-label="delete" style={{color:"red"}}>
<DeleteIcon />
</IconButton>
</button>
<button onClick={e => likef(item.id)} >     <IconButton aria-label="add to favorites">
<FavoriteIcon />
</IconButton>Like {item.likes}</button>
<button onClick={e => DeleteMovie(item.id)}>      <IconButton aria-label="add to favorites">
<FavoriteIcon />
</IconButton>DisLike {item.dislikes}</button>


                  </CardActions>
                  </Card>
                   

                 </Grid>
                 </div>
                  

                

                )
              }
              )

            }

                 </Grid>
{         
          movies.slice(page * RowsPerPage,page * RowsPerPage +RowsPerPage)
}               <TablePagination 
               rowsPerPageOptions={[1,2]}
               component="div"
               page={page}
               onPageChange={handleChangePage}
               onRowsPerPageChange={handleChangeRowPerpage}/>

          </div>

        

  )
}  
export default Movies;
